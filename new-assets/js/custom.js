window.onscroll = function() { myFunction() };
var header = document.getElementById("main-header");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

$('#main-menu-button').click(function() {
    $('.navbar-outer').toggleClass('open-side-menu');
    $('html,body').toggleClass('fixed-body');
    $(this).toggleClass('close-menu-open');
    $('#blank-div').toggleClass('display-blank-div');
});

$('#blank-div').click(function() {
    $('.navbar-outer').removeClass('open-side-menu');
    $('html,body').removeClass('fixed-body');
    $('#main-menu-button').removeClass('close-menu-open');
    $('#blank-div').removeClass('display-blank-div');
});

$('.holly-story-content').readmore({
    speed: 75,
    moreLink: '<a class="web-btn holly-story-link" href="javascript:void(0)">Read more <i class="arrow1 las la-angle-right"></i></a>',
    lessLink: '<a class="web-btn holly-story-link" href="javascript:void(0)">Read less <i class="arrow1 las la-angle-right"></i></a>'
});

$.validator.setDefaults({
    submitHandler: function() {}
});
$().ready(function() {
    $("#conatctus-form").validate({
        rules: {
            name: "required",
            phone: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            phone: "Please enter your phone number",
            email: "Please enter your valid email address"
        }
    });
});