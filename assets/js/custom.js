window.onscroll = function() { myFunction() };
var header = document.getElementById("main-header");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

$('.partner-slider').slick({
    variableWidth: true,
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 3500,
    prevArrow: $('.prev01'),
    nextArrow: $('.next01'),
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            dots: true
        }
    }]
});

$('.member-think-slider').slick({
    variableWidth: true,
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 9000,
    prevArrow: $('.prev02'),
    nextArrow: $('.next02'),
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            dots: true
        }
    }]
});

$('.circle-bottom-slider').slick({
    prevArrow: $('.prev03'),
    nextArrow: $('.next03'),
    variableWidth: true,
    autoplay: true,
    centerMode: true,
    autoplaySpeed: 5000
});

$('.pc-banner-slider').slick({
    prevArrow: $('.prev04'),
    nextArrow: $('.next04'),
    autoplay: true,
    centerMode: true,
    autoplaySpeed: 3500,
    variableWidth: true,
    pauseOnFocus: false,
    pauseOnHover: false
});

$(function() {
    $('.circle-oppprtunites-slider').slick({
        prevArrow: $('.prev05'),
        nextArrow: $('.next05'),
        variableWidth: true,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 5000,
        mobileFirst: true,
        responsive: [{
            breakpoint: 767,
            settings: 'unslick'
        }]
    });
    $(window).on('resize', function() {
        $('.circle-oppprtunites-slider').slick('resize');
    });
});

$(function() {
    $('.circle-oppprtunites-slider1').slick({
        prevArrow: $('.prev06'),
        nextArrow: $('.next06'),
        variableWidth: true,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 5000,
        mobileFirst: true,
        responsive: [{
            breakpoint: 767,
            settings: 'unslick'
        }]
    });
    $(window).on('resize', function() {
        $('.circle-oppprtunites-slider1').slick('resize');
    });
});

$('.like-box').click(function() {
    $(this).toggleClass('active-box');
});

$(".all-property-slider").ionRangeSlider({
    min: 10000,
    max: 500000,
    type: 'single',
    prefix: "$",
    prettify_enabled: 'true',
    prettify_separator: ',',
    from: 500000
});

$(".deposit-slider").ionRangeSlider({
    min: 10000,
    max: 500000,
    step: 1000,
    type: 'single',
    prefix: "$",
    prettify_enabled: 'true',
    prettify_separator: ','
});

$(".investor-slider").ionRangeSlider({
    min: 1,
    max: 20,
    type: 'single',
    prettify_enabled: 'true',
    prettify_separator: ','
});

$(".goal-amount-slider").ionRangeSlider({
    min: 500,
    max: 500000,
    step: 100,
    type: 'single',
    prefix: "$",
    prettify_enabled: 'true',
    prettify_separator: ','
});

$(".recurring-slider").ionRangeSlider({
    min: 50,
    max: 500000,
    step: 50,
    type: 'single',
    prefix: "$",
    prettify_enabled: 'true',
    prettify_separator: ','
});

/* var range = $('.deposit-slider'),
    value = $('.deposit-value');

value.val(range.attr('value'));

range.on('input', function() {
    value.val(this.value);
}); */

/* var range1 = $('.investor-slider'),
    value1 = $('.investor-value');

value1.val(range1.attr('value'));

range1.on('input', function() {
    value1.val(this.value);
}); */

$(".show-password, .hide-password").on('click', function() {
    var passwordId = $(this).parents('.form-group').find('input').attr('id');
    if ($(this).hasClass('show-password')) {
        $("#" + passwordId).attr("type", "text");
        $(this).parent().find(".show-password").hide();
        $(this).parent().find(".hide-password").show();
    } else {
        $("#" + passwordId).attr("type", "password");
        $(this).parent().find(".hide-password").hide();
        $(this).parent().find(".show-password").show();
    }
});



$.validator.setDefaults({
    submitHandler: function() {}
});
$().ready(function() {
    $("#signup-popup-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            importantWarning: "required",
            privacyPolicy: "required"
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            importantWarning: "Please check this field",
            privacyPolicy: "Please check this field"
        }
    });
    $("#signup-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            importantWarning: "required",
            privacyPolicy: "required"
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            importantWarning: "Please check this field",
            privacyPolicy: "Please check this field"
        }
    });
    $("#login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            email: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            }
        }
    });
    $("#forgot-password-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: "Please enter your valid email address"
        }
    });
    $("#join-circle-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter your valid email address"
        }
    });
    $("#saving-paln-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter your valid email address"
        }
    });
    $("#get-touch-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter your valid email address"
        }
    });
    $("#conatctus-form").validate({
        rules: {
            name: "required",
            phone: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            phone: "Please enter your phone number",
            email: "Please enter your valid email address"
        }
    });
});


$('.signup-popup .close').on('click', function() {
    $('#signup-form').trigger("reset");
});

$('#main-menu-button').click(function() {
    $('.navbar-outer').toggleClass('open-side-menu');
    $('html,body').toggleClass('fixed-body');
    $(this).toggleClass('close-menu-open');
    $('#blank-div').toggleClass('display-blank-div');
});

$('#blank-div').click(function() {
    $('.navbar-outer').removeClass('open-side-menu');
    $('html,body').removeClass('fixed-body');
    $('#main-menu-button').removeClass('close-menu-open');
    $('#blank-div').removeClass('display-blank-div');
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 400) {
        $('.back-to-top').fadeIn();
    } else {
        $('.back-to-top').fadeOut();
    }
});
$('.back-to-top').click(function() {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
});

if ($('.datecontrol').length > 0) {
    $(".datecontrol").datepicker();

}

$(function() {
    $('a[href="how-it-works.html#exit-point"]').on('click', function() {
        $('.main-tab-section #nav-tab .nav-link').removeClass('active');
        $('.main-tab-section #nav-tab #nav-start-own-tab.nav-link').addClass('active');
        $('.main-tab-section #nav-tabContent .tab-pane.fade').removeClass('active show');
        $('.main-tab-section #nav-tabContent #nav-start-your-own.tab-pane.fade').addClass('active show');
        $('#StartYourOwn .card .card-header h4').addClass('collapsed');
        $('#StartYourOwn #exit-point.card .card-header h4').removeClass('collapsed');
        $('#StartYourOwn .card .collapse').removeClass('show');
        $('#StartYourOwn #exit-point.card .collapse').addClass('show');
    });


    $('ul.footer-links a[href*=\\#]:not([href=\\#]), .webinar-bottom-content a.web-btn').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                var header_height = $('#main-header').innerHeight();
                $('html,body').animate({
                    scrollTop: target.offset().top - header_height
                }, 500);
                return false;
            }
        }
    });


    /* var urlHash = window.location.href.split("#")[1];
    if (urlHash && $('#' + urlHash).length) {
        $('#StartYourOwn .card .card-header h4').addClass('collapsed');
        $('#StartYourOwn #exit-point.card .card-header h4').removeClass('collapsed');
        $('#StartYourOwn .card .collapse').removeClass('show');
        $('#StartYourOwn #exit-point.card .collapse').addClass('show');
        $('html,body').animate({
            scrollTop: $('#' + urlHash).offset().top - $('#main-header').innerHeight()
        }, 500, function() {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = urlHash;
        });
    } */


    if (window.location.hash) {
        // smooth scroll to the anchor id
        $('#StartYourOwn .card .card-header h4').addClass('collapsed');
        $('#StartYourOwn #exit-point.card .card-header h4').removeClass('collapsed');
        $('#StartYourOwn .card .collapse').removeClass('show');
        $('#StartYourOwn #exit-point.card .collapse').addClass('show');
        $('html,body').animate({
            scrollTop: $(window.location.hash).offset().top - $('#main-header').height()
        }, 500, 'swing');
    }


    if (window.history.pushState) {
        window.history.pushState('', '/', window.location.pathname)
    } else {
        window.location.hash = '';
    }

});


$('.bottom-three-buttons #circle-vision').on('click', function() {
    $('.pc-main-tab-section #nav-tab .nav-link').removeClass('active');
    $('.pc-main-tab-section #nav-tab #vision-tab.nav-link').addClass('active');
    $('.pc-main-tab-section #nav-tabContent .tab-pane.fade').removeClass('active show');
    $('.pc-main-tab-section #nav-tabContent .tab-pane.fade.vision-tab-content').addClass('active show');
    $('html,body').animate({
        scrollTop: $('.pc-main-tab-section').offset().top - $('header').innerHeight()
    }, 500);
});

$('.bottom-three-buttons #important-documents').on('click', function() {
    $('.pc-main-tab-section #nav-tab .nav-link').removeClass('active');
    $('.pc-main-tab-section #nav-tab #important-document-tab.nav-link').addClass('active');
    $('.pc-main-tab-section #nav-tabContent .tab-pane.fade').removeClass('active show');
    $('.pc-main-tab-section #nav-tabContent .tab-pane.fade.document-tab-content').addClass('active show');
    $('html,body').animate({
        scrollTop: $('.pc-main-tab-section').offset().top - $('header').innerHeight()
    }, 500);
});



function start_quiz_slider() {
    var slider = $('.quiz-slider').slick({
        infinite: false,
        draggable: false,
        touchMove: false,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        appendDots: $('.slideControls'),
        dots: true,
        customPaging: function(slider, i) {
            var thumb = jQuery(slider.$slides[i]).data();
            return '<a>' + ('' + (i + 1)).slice(-2) + '</a>' + ' ' + '/' + " " + slider.slideCount;
        },

        fnCanGoNext: function(instance, currentSlide) {
            var currentSlide = instance.$slides.eq(currentSlide);
            var checkbox = currentSlide.find("input[type=checkbox]");
            var radio = currentSlide.find("input[type=radio]");
            var selectBox = currentSlide.find("select#available-deposit option");

            if (checkbox.is(':checked') || radio.is(':checked') || selectBox.is(':selected')) {
                return true;
            } else {
                return false;
            }
        },
        responsive: [{
            breakpoint: 768,
            settings: {
                adaptiveHeight: true
            }
        }]


    });

    /* $('#quiz-start-btn').click(function() {
        var indexVal = $(this).data('id');
        slider.slick('slickGoTo', indexVal);
    }); */

    $('#interested-in input').on('click', function() {
        $('#submit-result').click(function() {
            $('#slider-outer').hide();
            $('#quiz-result-part').show();
        });
    });
}


$('#quiz-start-btn').click(function() {
    $('#quiz-start-section').hide();
    setTimeout(function() {
        $('#quiz-outer-section').show();
        start_quiz_slider()
    }, 1);
});


function quiz_slider() {
    var availableDeposit = document.getElementById('available-deposit').value;

    var personalCashFlow = document.getElementById('personal-cash-flow').value;
    var managedSuperFund = document.getElementById('managed-super-fund').value;
    var familyTrush = document.getElementById('family-trush').value;
    var personalLoan = document.getElementById('personal-loan').value;
    var company = document.getElementById('company').value;
    var fundsOther = document.getElementById('funds-other').value;

    var oneYear = document.getElementById('one-year').value;
    var fiveYears = document.getElementById('five-years').value;
    var tenYears = document.getElementById('ten-years').value;
    var longer = document.getElementById('longer').value;

    var residentialInvest = document.getElementById('residential-invest').value;
    var residentialTenant = document.getElementById('residential-tenant').value;
    var commercialProperty = document.getElementById('commercial-property').value;
    var developmentProject = document.getElementById('development-project').value;
    var renovationProject = document.getElementById('renovation-project').value;
    var lifestyle = document.getElementById('lifestyle').value;
    var propertyOther = document.getElementById('property-other').value;

    var positivelyGeared = document.getElementById('positively-geared').value;
    var negativelyGeared = document.getElementById('negatively-geared').value;
    var notImportant = document.getElementById('not-important').value;

    var renovatingProperties = document.getElementById('renovating-properties').value;
    var propertyDevelopment = document.getElementById('property-development').value;
    var propertyPortfolio = document.getElementById('property-portfolio').value;
    var shortTermRental = document.getElementById('short-term-rental').value;
    var creatingOwnProperty = document.getElementById('creating-own-property').value;


    var renovationCircle = false;
    var startupCircle = false;
    var dreamCircle = false;
    var developmentCircle = false;
    var commericialCircle = false;


    switch (availableDeposit) {
        case '10000':
            startupCircle = true;
            break;
        case '20000':
            startupCircle = true;
            commericialCircle = true;
            break;
        case '30000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            break;
        case '40000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            break;
        case '50000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            break;
        case '60000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            break;
        case '70000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            break;
        case '80000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
            break;
        case '90000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
            break;
        case '100000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
            break;
        case '110000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
            break;
        case '120000':
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
            break;
        default:
            startupCircle = true;
            break;
    }


    if (personalCashFlow) {
        startupCircle = true;
        commericialCircle = true;
        developmentCircle = true;
        renovationCircle = true;
        dreamCircle = true;
    } else {
        if (managedSuperFund) {
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
        } else {
            if (familyTrush) {
                startupCircle = true;
                commericialCircle = true;
                developmentCircle = true;
                renovationCircle = true;
                dreamCircle = true;
            } else {
                if (personalLoan) {
                    startupCircle = true;
                    commericialCircle = true;
                    developmentCircle = true;
                    renovationCircle = true;
                    dreamCircle = true;
                } else {
                    if (company) {
                        startupCircle = true;
                        commericialCircle = true;
                        developmentCircle = true;
                        renovationCircle = true;
                        dreamCircle = true;
                    } else {
                        if (fundsOther) {
                            startupCircle = true;
                            commericialCircle = true;
                            developmentCircle = true;
                            renovationCircle = true;
                            dreamCircle = true;
                        }
                    }
                }
            }
        }
    }


    if (oneYear) {
        developmentCircle = true;
        renovationCircle = true;
    } else {
        if (fiveYears) {
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
            dreamCircle = true;
        } else {
            if (tenYears) {
                startupCircle = true;
                commericialCircle = true;
                dreamCircle = true;
            } else {
                if (longer) {
                    startupCircle = true;
                    commericialCircle = true;
                    dreamCircle = true;
                }
            }
        }
    }


    if (residentialInvest) {
        startupCircle = true;
        renovationCircle = true;
    } else {
        if (residentialTenant) {
            startupCircle = true;
        } else {
            if (commercialProperty) {
                commericialCircle = true;
            } else {
                if (developmentProject) {
                    developmentCircle = true;
                } else {
                    if (renovationProject) {
                        renovationCircle = true;
                    } else {
                        if (lifestyle) {
                            startupCircle = true;
                            renovationCircle = true;
                            dreamCircle = true;
                        } else {
                            if (propertyOther) {
                                startupCircle = true;
                                dreamCircle = true;
                            }
                        }
                    }
                }
            }
        }
    }


    if (positivelyGeared) {
        startupCircle = true;
        commericialCircle = true;
        renovationCircle = true;
    } else {
        if (negativelyGeared) {
            startupCircle = true;
            commericialCircle = true;
            developmentCircle = true;
            renovationCircle = true;
        } else {
            if (notImportant) {
                startupCircle = true;
                commericialCircle = true;
                developmentCircle = true;
                renovationCircle = true;
                dreamCircle = true;
            }
        }
    }

    if (renovatingProperties) {
        renovationCircle = true;
    } else {
        if (propertyDevelopment) {
            developmentCircle = true;
            renovationCircle = true;
        } else {
            if (propertyPortfolio) {
                startupCircle = true;
                commericialCircle = true;
                dreamCircle = true;
            } else {
                if (shortTermRental) {
                    startupCircle = true;
                    dreamCircle = true;
                } else {
                    if (creatingOwnProperty) {
                        startupCircle = true;
                        dreamCircle = true;
                    }
                }
            }
        }
    }



    if (startupCircle) {
        document.getElementById('startup-circle-box').style.display = 'flex';
    }
    if (commericialCircle) {
        document.getElementById('commercial-circle-box').style.display = 'flex';
    }
    if (developmentCircle) {
        document.getElementById('development-circle-box').style.display = 'flex';
    }
    if (renovationCircle) {
        document.getElementById('renovation-circle-box').style.display = 'flex';
    }
    if (dreamCircle) {
        document.getElementById('dream-circle-box').style.display = 'flex';
    }

};


function saving_calculate() {
    var goal_amount = document.getElementById('goal-amount-slider').value;
    var start_date = document.getElementById('start_date').value;
    var recurring_amount = document.getElementById('recurring-slider').value;
    var frequency = document.getElementById('frequency').value;
    /* var interest_rate = document.getElementById('interest_rate').value; */

    var monthly_recurring_amount = 0;

    switch (frequency) {
        case 'weekly':
            monthly_recurring_amount = recurring_amount * 4.33;
            break;
        case 'monthly':
            monthly_recurring_amount = recurring_amount * 1;
            break;
        case 'quarterly':
            monthly_recurring_amount = recurring_amount / 3;
            break;
        case 'yearly':
            monthly_recurring_amount = recurring_amount / 12;
            break;
        default:
            monthly_recurring_amount = recurring_amount;
            break;
    }



    var term_in_month = Math.ceil(goal_amount / monthly_recurring_amount);
    end_date_var = new Date(start_date);
    end_date_var.setMonth(end_date_var.getMonth() + term_in_month + 1);
    document.getElementById('reach_goal').value = end_date_var.getMonth() + '/' + end_date_var.getDate() + '/' + end_date_var.getFullYear();

    var labels = [];
    var amount = [];
    var interestcummalative = [];

    var currentAmount = 0;
    var interestAdded = 0;
    for (var i = 1; i < term_in_month + 1; i++) {
        var date_object = new Date(start_date);
        date_object.setMonth(date_object.getMonth() + i);
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][date_object.getMonth()];
        var final_date = month + '-' + date_object.getFullYear();
        labels.push(final_date);
        currentAmount = currentAmount + monthly_recurring_amount;
        amount.push(currentAmount);

        if (date_object.getMonth() === 11) {
            interestAdded = (currentAmount * 2.5) / 100;
        } else {}
        interestcummalative.push(interestAdded + currentAmount);
    }


    var ctx = document.getElementById('SavingGoal').getContext('2d');
    var SavingGoal = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Interest Cumulative',
                data: interestcummalative,
                backgroundColor: [
                    '#97D3D3'
                ],
                borderColor: [
                    '#2CA2AC'
                ],
                borderWidth: 1,
                fill: 1
            }, {
                label: 'Amounts',
                data: amount,
                backgroundColor: [
                    '#C7EAE5'
                ],
                borderColor: [
                    '#45B9A8'
                ],
                borderWidth: 1,
                fill: 'origin'
            }]
        },
        options: {}
    });
}


function property_estimation() {
    var estimated_property_value = document.getElementById('estimated_property_value').value.replace(/,/g, "");
    var stamp_duty = document.getElementById('stamp_duty').value.replace(/,/g, "");
    var mortgage = document.getElementById('mortgage').value;
    var other_cost = document.getElementById('other_cost').value.replace(/,/g, "");
    var mortgage_type = document.getElementById('mortgage_type').value;
    var additional_cost = document.getElementById('additional_cost').value.replace(/,/g, "");
    var interest = document.getElementById('interest').value;
    var other_annual_cost = document.getElementById('other_annual_cost').value.replace(/,/g, "");



    var mortgage_amount = estimated_property_value * mortgage / 100;
    document.getElementById('mortgage_amount').value = formatDollar(mortgage_amount);


    var total_fund_required = estimated_property_value - mortgage_amount + parseInt(other_cost) + parseInt(stamp_duty) + parseInt(additional_cost);

    document.getElementById('total_fund_required').value = formatDollar(total_fund_required);

    var investor_range = document.getElementById('investor-slider').value;

    document.getElementById('investor-value').value = investor_range;

    document.getElementById('amount-investor').value = formatDollar(total_fund_required / investor_range);


    //Aneesh Sir code start here

    var rental_income_cover = 21;
    var week_to_cover = rental_income_cover * 52;
    var weekly_rent = estimated_property_value / week_to_cover;
    var annual_rent = weekly_rent * 52;
    var annual_rental_income = annual_rent;
    document.getElementById('annual_rental_income').value = formatDollar(annual_rental_income);
    var annual_morgage_costs = 0;
    if (mortgage_type == 'Principal_interest') {
        annual_morgage_costs = pmt((interest / 1200), 360, -mortgage_amount, 0, 0) * 12;
    } else {
        annual_morgage_costs = mortgage_amount * (interest / 1200) * 12;
    }
    document.getElementById('annual_mortgage_cost').value = formatDollar(annual_morgage_costs);
    document.getElementById('total_annual_return').value = formatDollar(annual_rental_income - annual_morgage_costs - other_annual_cost);
    document.getElementById('total_annual_return_per-investor').value = formatDollar((annual_rental_income - annual_morgage_costs - other_annual_cost) / investor_range);

}


function circle_details() {
    var property_value = document.getElementById('property_value').value;
    var investment_term = document.getElementById('investment_term').value;
    var estimated_annual_gain = document.getElementById('estimated_annual_gain').value;
    var deposit_available = document.getElementById('deposit-slider').value;
    var deposit_value = document.getElementById('deposit-slider').value;

    var deposit_required = property_value * 20 / 100;

    document.getElementById('deposit-required').value = formatDollar(deposit_required);

    document.getElementById('deposit-value').value = deposit_value;


    document.getElementById('average_rental_yield').value = (((property_value / (21 * 52)) * 52 / property_value) * 100).toFixed(2);



    var Annualised_Percentage_of_Return = ((5.5) / investment_term) / 100;
    var Estimated_Annual_Capital_Gain_on_Property = property_value * Annualised_Percentage_of_Return;

    var my_share = (deposit_value / (property_value * 20 / 100)) * 100;


    var estimated_annual_capital_gain_on_property = 0;

    if (estimated_annual_gain <= Annualised_Percentage_of_Return) {
        estimated_annual_capital_gain_on_property = (Estimated_Annual_Capital_Gain_on_Property * my_share * investment_term) / 100;

    } else {
        estimated_annual_capital_gain_on_property = (Estimated_Annual_Capital_Gain_on_Property * my_share * investment_term) / 100;
        estimated_annual_capital_gain_on_property = estimated_annual_capital_gain_on_property * (estimated_annual_gain / 100) + estimated_annual_capital_gain_on_property;
    }


    document.getElementById('estimated_annual_capital_gain_on_property').value = formatDollar(estimated_annual_capital_gain_on_property);



    var To_cover_property_value_in_21_years = 1092;
    var Weekly_Rent = property_value / To_cover_property_value_in_21_years;
    var Rental_Yield = (Weekly_Rent * 52 / property_value) / 100;
    var Annual_Rental_Income = Rental_Yield * property_value;

    var Estimated_Annual_Rental_Income = Annual_Rental_Income * my_share * investment_term;

    var total_gain_after_investment_term = Estimated_Annual_Rental_Income + estimated_annual_capital_gain_on_property;


    document.getElementById('estimated_annual_rental_income').value = formatDollar(Estimated_Annual_Rental_Income);
    document.getElementById('total_gain_after_investment_term').value = formatDollar(total_gain_after_investment_term);

    var net_gain = total_gain_after_investment_term - deposit_value;
    document.getElementById('net_gain_after_investment_term').value = formatDollar(net_gain);


    var d = new Date();

    var labels = [];
    var Max_of_Expected_Annual_Gain_on_Property = [];
    var Sum_of_Expected_Rental_Income = [];
    var Sum_of_Net_Gain = [];


    for (var i = -1; i < 5; i++) {

        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();

        var c = new Date(year + i, month, day);
        var date_label = c.getDate() + "/" + (c.getMonth() + 1) + "/" + c.getFullYear();
        labels.push(date_label);

        if (i == -1) {
            Max_of_Expected_Annual_Gain_on_Property.push(0);
            Sum_of_Expected_Rental_Income.push(0);
            Sum_of_Net_Gain.push(0);
        } else {
            Max_of_Expected_Annual_Gain_on_Property.push(Math.ceil(estimated_annual_capital_gain_on_property * (i + 1)));
            Sum_of_Expected_Rental_Income.push(Math.ceil(Estimated_Annual_Rental_Income * (i + 1)));
            Sum_of_Net_Gain.push(Math.ceil(net_gain * (i + 1)));

        }

    }


    var ctx = document.getElementById('CircleDetail').getContext('2d');
    var CircleDetail = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Max of  Expected Annual Gain on Property ',
                data: Max_of_Expected_Annual_Gain_on_Property,
                borderColor: [
                    '#6BBBFE'
                ],
                borderWidth: 1,
                fill: false,

            }, {
                label: 'Sum of Expected Rental Income ',
                data: Sum_of_Expected_Rental_Income,
                borderColor: [
                    '#2EB470'
                ],
                borderWidth: 1,
                fill: false
            }, {
                label: 'Sum of Net Gain ',
                data: Sum_of_Net_Gain,
                borderColor: [
                    '#B35297'
                ],
                borderWidth: 1,
                fill: false
            }]
        },
        options: {}
    });

}


function format(input) {
    var nStr = input.value + '';
    nStr = nStr.replace(/\,/g, "");
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    input.value = x1 + x2;
};


/**
 * Copy of Excel's PMT function.
 * Credit: http://stackoverflow.com/questions/2094967/excel-pmt-function-in-js
 *
 * @param rate_per_period       The interest rate for the loan.
 * @param number_of_payments    The total number of payments for the loan in months.
 * @param present_value         The present value, or the total amount that a series of future payments is worth now;
 *                              Also known as the principal.
 * @param future_value          The future value, or a cash balance you want to attain after the last payment is made.
 *                              If fv is omitted, it is assumed to be 0 (zero), that is, the future value of a loan is 0.
 * @param type                  Optional, defaults to 0. The number 0 (zero) or 1 and indicates when payments are due.
 *                              0 = At the end of period
 *                              1 = At the beginning of the period
 * @returns {number}
 */
function pmt(rate_per_period, number_of_payments, present_value, future_value, type) {
    future_value = typeof future_value !== 'undefined' ? future_value : 0;
    type = typeof type !== 'undefined' ? type : 0;

    if (rate_per_period != 0.0) {
        // Interest rate exists
        var q = Math.pow(1 + rate_per_period, number_of_payments);
        return -(rate_per_period * (future_value + (q * present_value))) / ((-1 + q) * (1 + rate_per_period * (type)));

    } else if (number_of_payments != 0.0) {
        // No interest rate, but number of payments exists
        return -(future_value + present_value) / number_of_payments;
    }

    return 0;
};


function formatDollar(num) {
    var parts = num.toFixed(2).split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};
$('body').on('hidden.bs.modal', '.modal', function() {
    $(myVideo).trigger('pause');
});
$('body').on('shown.bs.modal', '.modal', function() {
    $(myVideo).trigger('play');
});